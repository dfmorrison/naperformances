#!/usr/bin/python3

# Stupid IDLE doesn't yet support command line arguments from its GUI (looks like they've
# been "working on it" for at least eight years, sheesh). This cobbles around the
# problem without tinkering inside naperformances.py.

import sys

# Uncomment and adjust the following if necessary
# sys.path.insert(0, "/Users/whoever/mumble/")

import naperformances

if not sys.argv[1:]:
    # Edit to taste.
    sys.argv += ["--from", "2016-04-08", "--sort", "chronological"]

def main():
    naperformances.main()

if __name__ == '__main__':
    main()
