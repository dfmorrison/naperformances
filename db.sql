# The two tables in the MySQL database that are written are defined as follows:
CREATE TABLE performances (
  # the id comes from Bellboard
  id INT UNSIGNED NOT NULL,
  kind ENUM('touch', 'quarter', 'peal') NOT NULL,
  medium ENUM('tower', 'hand') NOT NULL,
  guild VARCHAR(100),
  place VARCHAR(150),
  county VARCHAR(100),
  dedication VARCHAR(100),
  location VARCHAR(250),
  pdate DATE NOT NULL,
  duration VARCHAR(20),
  tenor VARCHAR(20),
  length MEDIUMINT UNSIGNED NOT NULL,
  # the method includes the stage
  method VARCHAR(255),
  details TEXT,
  composer VARCHAR(255),
  footnotes TEXT,
  composition TEXT,
  formatted MEDIUMTEXT NOT NULL,
  entered TIMESTAMP NOT NULL,
  updated DATE,
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

CREATE TABLE ringers (
  id INT UNSIGNED NOT NULL,
  bells VARCHAR(5) NOT NULL,
  name VARCHAR(100) NOT NULL,
  conductor BOOLEAN NOT NULL,
  PRIMARY KEY (id, bells)
) DEFAULT CHARSET=utf8;
