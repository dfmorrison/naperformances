#!/usr/bin/python3

# Copyright (c) 2015-2023 Donald F Morrison, Austin Paul
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included in all copies
# or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# While this will probably work with any Python 3 version, it was tested most recently
# with 3.9.

import argparse, collections, datetime, itertools, json, locale, logging, re, sys, time, warnings
import urllib.parse, urllib.request, xml.etree.ElementTree

CREDENTIALS = "/etc/naperformances_credentials.py"
LOGFILE = "/var/log/naperformances/db.log"
DAYSBACK = 120
PAGESIZE = 10000
BELLBOARD = "https://bb.ringingworld.co.uk/"
NAMESPACE = "http://bb.ringingworld.co.uk/NS/performances#"
RETRIES = 3
RETRYTIME = 10

database = None
dbUser = None
dbPassword = None

if CREDENTIALS:
    try:
        with open(CREDENTIALS) as f:
            # typically redefines database, dbUser and dbPassword
            exec(f.read())
    except FileNotFoundError:
        pass

if database:
    try:
        import pymysql
    except ImportError:
        database = None
        warnings.warn("Couldn't import pymysql, database use suppressed")

association_words = ["American", "Abraham", "MIT", "Massachusetts", "Washington", "WRS"]
place_words = []
county_words = []
place_modifiers = {}

modifiers = collections.namedtuple("modifiers", ["include_re", "exclude_re"])

def splitcomma(s):
    return re.split("[ \t]*,[ \t]*", s)

def regex(words):
    return re.compile(r"\b(?:{})\b".format("|".join(words)), re.IGNORECASE)

def places(names, include=None, exclude=None, county=False):
    names = splitcomma(names.lower())
    place_words.extend(names)
    if county:
        county_words.extend(names)
    if include or exclude:
        mod = modifiers(regex(splitcomma(include.lower())) if include else None,
                        regex(splitcomma(exclude.lower())) if exclude else None)
        for name in names:
            place_modifiers[name] = mod

places("Canada, United States, America, US, USA, U.S, Mexico, Virgin Islands, Bahamas",
       county=True)
places("BC, MB, NL, NS, NT, NU, PE, QC, SK, YT", county=True)
places("AB", exclude="Kettleby, Leics, Leicestershire", county=True)
places("NB", exclude="Intrepid, Copperkins, Canal, Stedman", county=True)
places("Alberta, British Columbia, Manitoba, New Brunswick, Newfoundland, Labrador,"
       "Nova Scotia, Northwest Territories, Nunavut, Ontario, Prince Edward Island,"
       "PEI, Quebec, Québec, Saskatchewan, Yukon", county=True)
places("AL, AK, AZ, CF, CL, CT, DL, DC, FL, GA, HA, ID, IL, IA, KA, KY, ME,"
       "MD, MS, MC, MN, MI, MO, MT, NV, NH, NJ, NM, NY, NC, ND, OH, OK, OR, PA, RI,"
       "SC, SD, TN, TX, UT, VT, VA, WN, WV, WS, NYC", county=True)
places("AR", exclude="-ar-", county=True)
places("WY", exclude="ar", county=True)
places("MS", exclude="Bristol", county=True)
places("MT", exclude="Australia", county=True)
places("WA", exclude="Bunbury, Mandurah, Perth, Rockingham, York", county=True)
places("Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,"
       "District of Columbia, Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa,"
       "Kansas, Kentucky, Louisiana, Maine, Maryland, Massachusetts, Michigan, Minnesota,"
       "Mississippi, Missouri, Montana, Nebraska, Nevada, New Hampshire, New Jersey,"
       "New Mexico, New York, North Carolina, North Dakota, Ohio, Oklahoma, Oregon,"
       "Pennsylvania, Rhode Island, South Carolina, South Dakota, Tennessee, Texas,"
       "Utah, Vermont, Virginia, West Virginia, Wisconsin, Wyoming",
       county=True)
places("Washington", exclude="Sussex, Mary", county=True)
places("Abilene, Alexandria, Atlanta, Augusta, Brewster, Calgary, Carmel, Charleston,"
       "Chicago, Dallas, Eugene, Frederick, Groton, Hendersonville, Honolulu, Houston,"
       "Kalamazoo, Little Rock, Marietta, Miami, Mission, MIT, Orleans, Philadelphia,"
       "Pittsburgh, Princess Anne, Riverside, Seattle, Sewanee, Shreveport, Sullivan,"
       "Texarkana, Toronto, Vancouver, Watertown, Burlington, Kent School, Wall Street")
places("Marblehead, Foxborough, Fairfax, Arlington, Chevy, Annapolis, San Diego")
places("Boston", exclude="Botolph")
places("Hingham", include="Memorial")
places("New Castle", include="Immanuel")
places("Northampton", include="Mendenhall, Smith")
places("Raleigh", exclude="Devon, Colaton, Combe, Withycombe")
places("Rochester", exclude="Kent, Cathedral, Christ, Margaret, Stephen, Westminster")
places("VTS")
places("Victoria",
       exclude="Ballarat, Beechworth, Bendigo, Burnley, Geelong, Melbourne, "
               "Wangaratta, Heidelberg, Heidleberg, Australia",
       county=True)

association_regex = None

def getPerformances(queryParams, verbose=False):
    """An iterator over performances downloaded from Bellboard for a query with the
    paramaters in the dict queryParams. The returned values are ElementTrees corresponding
    to a performance element."""
    url = "{}export.php?{}&pagesize={}&page=".format(
        BELLBOARD, urllib.parse.urlencode(queryParams), PAGESIZE)
    total = 0
    for page in itertools.count(1):
        for i in range(RETRIES+1):
            try:
                response = urllib.request.urlopen(
                    urllib.request.Request(url + str(page),
                                           headers={"Accept":"application/xml"}))
                if verbose:
                    print(url + str(page))
            except IOError:
                raise RuntimeError("Can't reach Bellboard")
            if response.status == 200:
                break
            time.sleep(RETRYTIME)
        else:
            raise RuntimeError("Unexpected response from server ({}): {}".format(
                response.status, response.reason))
        performances = xml.etree.ElementTree.fromstring(
            decontrolify(response.read().decode("utf-8")))
        if performances.tag != ns("performances"):
            raise RuntimeError("Unexpected tag: {}".format(performances.tag))
        for p in performances.findall(ns("performance")):
            total += 1
            yield p
        if total == 0 or total % PAGESIZE != 0:
            return

def getComposition(id):
    url = "{}comp.php?id={}".format(BELLBOARD, id)
    for i in range(RETRIES+1):
        try:
            response = urllib.request.urlopen(
                urllib.request.Request(url, headers={"Accept":"text/plain"}))
        except IOError:
            raise RuntimeError("Can't reach Bellboard")
        if response.status == 200:
            break
        time.sleep(RETRYTIME)
    else:
        raise RuntimeError("Unexpected response from server ({}): {}".format(
            response.status, response.reason))
    return response.read().decode("utf-8")

def isNorthAmerican(performance):
    global association_regex, place_regex, county_regex
    if not association_regex:
        association_regex = regex(association_words)
        place_regex = regex(place_words)
        county_regex = regex(county_words)
    association = safeText(performance.find(ns("association")))
    if association and association_regex.search(canonicalize(association)):
        return True
    place = county = dedication = ""
    for p in performance.findall(ns("place", "place-name")):
        t = p.get("type")
        if t == "place":
            place = canonicalize(p.text)
        elif t == "county":
            county = canonicalize(p.text)
        elif t == "dedication":
            dedication = canonicalize(p.text)
    for word in itertools.chain(county_regex.findall(county), place_regex.findall(place)):
        mod = place_modifiers.get(word.lower())
        if not mod or ((not mod.include_re or
                        (mod.include_re.search(place) or
                         mod.include_re.search(dedication)))
                       and
                       (not mod.exclude_re or
                        (not mod.exclude_re.search(place) and
                         not mod.exclude_re.search(dedication)))):
            return True
    return False

def ns(*names):
    """Annotate names using the syntax etree uses for namespaces"""
    # incrutability alert: in a format string {{ = literal {, ditto }}
    return "/".join(map(lambda s: "{{{}}}{}".format(NAMESPACE, s), names))

def canonicalize(s):
    return re.sub("[ \t]+", " ", s) if s else ""

def safeText(tree):
    result = tree.text if tree is not None else None
    return result.strip() if result is not None else None

def decontrolify(s):
    """Strips out all C0 (7-bit ASCII) control codes that are not legal in XML 1.0.
    For mysterious reasons Bellboard occasionally serves XML that contains these."""
    return re.sub("[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+", "", s)

def parseDate(s):
    m = re.match("(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)", s)
    return datetime.date(int(m.group(1)), int(m.group(2)), int(m.group(3))) if m else None


class Performance:

    def __init__(self, tree):
        i = tree.get("id")
        m = re.match("^P(\\d+)$", i) if i else None
        Performance.testField(m, "id")
        self.bbid = int(m.group(1))
        self.association = safeText(tree.find(ns("association"))) or "Non Association"
        placeInfo = { p.get("type"): p.text.strip()
                      for p in tree.findall(ns("place", "place-name")) }
        self.place = placeInfo["place"] if "place" in placeInfo else None
        self.county = placeInfo["county"] if "county" in placeInfo else None
        self.dedication = placeInfo["dedication"] if "dedication" in placeInfo else None
        self.location = ""
        if self.place:
            self.location += self.place
        if self.county:
            self.location += "{}{}".format(", " if self.location else "", self.county)
        if self.dedication:
            self.location += "{}({})".format(" " if self.location else "", self.dedication)

        # This will pull virtual performances to the end in clapper sort, even if the formatting is different.
        self.clappersort_place = self.place
        if re.search("ringing\s*room", self.location, re.IGNORECASE):
            self.clappersort_place = "~ringingroom"
        elif re.search("handbell\s*stadium", self.location, re.IGNORECASE):
            self.clappersort_place = "~handbellstadium"

        self.date = parseDate(safeText(tree.find(ns("date"))))
        Performance.testField(self.date, "date")
        self.duration = safeText(tree.find(ns("duration")))
        self.tenor = None
        r = tree.find(ns("place", "ring"))
        if r is not None:
            t = r.get("tenor")
            if t:
                self.tenor = t.strip()
            self.medium = "hand" if r.get("type").strip() == "hand" else "tower"
        ln = safeText(tree.find(ns("title", "changes")))
        self.length = int(ln) if ln else 0
        self.kind = "touch" if self.length < 1250 else "quarter" if self.length < 5000 else "peal"
        self.method = safeText(tree.find(ns("title", "method")))
        self.details = safeText(tree.find(ns("details")))
        if self.details:
            # if the user added parens, strip them off so we don't end up with double ones
            m = re.match("^ *\\((.+)\\) *$", self.details)
            if m:
                self.details = m.group(1).strip()
        self.composer = safeText(tree.find(ns("composer")))
        if self.composer and re.match("Tradition|Anon|Unattribute", self.composer, re.I):
            self.composer = None
        if self.composer:
            c = tree.find(ns("composition"))
            if c is not None:
                n = c.get("name")
                if n:
                    self.composer += " ({})".format(n.strip())
        self.ringers = [ (str(r.get("bell")).strip(),
                          r.text.strip(),
                          r.get("conductor") == "true")
                         for r in tree.findall(ns("ringers", "ringer")) ]
        self.footnotes = "\n".join(f.text.strip()
                                   for f in tree.findall(ns("footnote")) )
        self.composition = None
        if self.kind == "peal":
            c = tree.find(ns("composition"))
            if c is not None:
                self.composition = getComposition(int(c.get("ref")))
        self.updated = None
        ts = safeText(tree.find(ns("timestamp")))
        if ts:
            self.updated = parseDate(ts)
            if not self.updated:
                raise ValueError("Unexpected timestamp format: {}".format(ts))

    def clapperSortKey(self):
        if self.kind == "peal":
            return (self.kind, self.association, self.date)
        else:
            return (self.kind, self.clappersort_place, self.date)
    def chronSortKey(self):
        return (self.kind, self.date)
    def bbidSortKey(self):
        return (self.kind, self.bbid)

    sort_styles = { "clapper":       clapperSortKey,
                    "chronological": chronSortKey,
                    "bbid":          bbidSortKey }

    def testField(value, name):
        if not value:
            raise ValueError("The performance has no {}".format(name))

    def toText(self):
        result = ""
        if self.association and self.kind == "peal":
            result += "{}\n".format(self.association)
        if self.place:
            result += self.place
            if self.county:
                result += ", {}".format(self.county)
            result += "\n"
        elif self.county:
            result += "{}\n".format(self.county)
        if self.dedication:
            result += "{}\n".format(self.dedication)
        result += "{:%A %-d %B %Y}".format(self.date)
        if self.duration:
            result += " in {}".format(self.duration)
        if self.tenor:
            result += " ({})".format(self.tenor)
        result += "\n{}".format(self.length)
        if self.method:
            result += " {}".format(self.method)
        result += "\n"
        if self.details:
            result += "({})\n".format(self.details)
        if self.composer:
            result += "comp. {}\n".format(self.composer)
        # Note that in the Clapper NAG peals are formatted differently than
        # non-NAG peals.
        if self.kind == "peal" and self.association == "North American Guild":
            for bells, name, isConductor in self.ringers:
                result += "\t{}\t{}{}\n".format(
                    bells, name, "\N{NO-BREAK SPACE}(C)" if isConductor else "")
        else:
            started = False
            for bells, name, isConductor in self.ringers:
                if started:
                    result += ", "
                result += "{}\N{NO-BREAK SPACE}{}{}".format(
                    bells, name, "\N{NO-BREAK SPACE}(C)" if isConductor else "")
                started = True
            result += ".\n"
        if self.footnotes:
            result += "{}\n".format(self.footnotes)
        if self.composition:
            result += "\nComposition:\n{}\n".format(self.composition)
        return result

    def toHtml(self):
        formatted = '<div class="performance">\n'
        if self.association:
            formatted += '<div class="perf_guild">{}</div>\n'.format(self.association)
        if self.location:
            formatted += '<div class="perf_loc">{}</div>\n'.format(self.location)
        formatted += '<div class="perf_date">On {:%A, %-d %B %Y}'.format(self.date)
        if self.duration:
            formatted += " in {}".format(self.duration)
        if self.tenor:
            formatted += " ({})".format(self.tenor)
        formatted += "</div>\n"
        formatted += '<div class="perf_meth"><a href="{}view.php?id={}">{}'.format(
            BELLBOARD, self.bbid, locale.format("%d", self.length, True))
        if self.method:
            formatted += " {}".format(self.method)
        formatted += "</a></div>\n"
        if self.details:
            formatted += '<div class="perf_details">({})</div>\n'.format(self.details)
        if self.composer:
            formatted += '<div class="perf_composer">{}</div>\n'.format(self.composer)
        formatted += '<table class="perf_band">\n'
        for bells, name, isConductor in self.ringers:
            formatted += '<tr><td class="perf_bells">{}</td><td class="perf_ringer">{}{}</td></tr>\n'.format(
                bells, name, " (C)" if isConductor else "")
        formatted += '</table>\n'
        if self.footnotes:
            formatted += '<div class="perf_foot">{}</div>\n'.format(self.footnotes)
        if self.updated:
            formatted += '<div class="perf_submit">Last revised on {:%-d %B %Y}</div>\n'.format(
                self.updated)
        return formatted + "</div>\n"
        # A possible style sheet for performances, as formatted above
        # .performance {
        #   line-height: 1.3;
        # }
        # .perf_guild {
        #   font-variant: small-caps;
        #   margin-bottom: 1.8ex;
        # }
        # .perf_loc {
        #   font-weight: bolder;
        # }
        # .perf_date {
        #   font-style: italic;
        # }
        # .perf_meth {
        #   font-weight: bolder;
        #   font-size: larger;
        #   margin-top: 0.6ex;
        # }
        # .perf_details {
        #   white-space: pre;
        # }
        # .perf_composer {
        # }
        # .perf_band {
        #   margin-left: 0.4em;
        # }
        # .perf_bells {
        #   text-align: right;
        # }
        # .perf_ringer {
        #   text-align: left;
        #   padding-left: 0.6em;
        # }
        # .perf_foot {
        #   white-space: pre;
        # }
        # .perf_submit {
        #   font-size: smaller;
        #   font-weight: lighter;
        #   font-style: italic;
        #   margin-top: 4ex;
        # }

# The two tables in the MySQL database that are written are defined as follows:
# CREATE TABLE performances (
#   # the id comes from Bellboard
#   id INT UNSIGNED NOT NULL,
#   kind ENUM('touch', 'quarter', 'peal') NOT NULL,
#   medium ENUM('tower', 'hand') NOT NULL,
#   guild VARCHAR(100),
#   place VARCHAR(150),
#   county VARCHAR(100),
#   dedication VARCHAR(100),
#   location VARCHAR(250),
#   pdate DATE NOT NULL,
#   duration VARCHAR(20),
#   tenor VARCHAR(20),
#   length MEDIUMINT UNSIGNED NOT NULL,
#   # the method includes the stage
#   method VARCHAR(255),
#   details TEXT,
#   composer VARCHAR(255),
#   footnotes TEXT,
#   composition TEXT,
#   formatted MEDIUMTEXT NOT NULL,
#   entered TIMESTAMP NOT NULL,
#   updated DATE,
#   PRIMARY KEY (id)
# ) DEFAULT CHARSET=utf8;
#
# CREATE TABLE ringers (
#   id INT UNSIGNED NOT NULL,
#   bells VARCHAR(5) NOT NULL,
#   name VARCHAR(100) NOT NULL,
#   conductor BOOLEAN NOT NULL,
#   PRIMARY KEY (id, bells)
# ) DEFAULT CHARSET=utf8;

def replacePerformances(fromDate, performances):
    conn = None
    cursor = None
    try:
        conn = pymysql.connect(db=database, user=dbUser, passwd=dbPassword, charset="utf8") #
        cursor = conn.cursor()
        # Once the MySQL version used on the NAG web site has been updated to version 5.5
        # or later this should be updated to be in a transaction so the deletion will be
        # rolled back if the insertions fail or whatever.
        try:
            cursor.execute("LOCK TABLES performances WRITE, ringers WRITE")
            if fromDate:
                cursor.execute("""DELETE ringers FROM ringers
                                  INNER JOIN performances ON performances.id = ringers.id
                                  WHERE performances.pdate >= %s""",
                               (fromDate,))
                cursor.execute("DELETE FROM performances WHERE pdate >= %s", (fromDate,))
            for p in performances:
                cursor.execute("DELETE FROM performances WHERE id = %s", (p.bbid,))
                cursor.execute("DELETE FROM ringers WHERE id = %s", (p.bbid,))
                cursor.execute("""INSERT INTO performances VALUES(
                                  %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                                  NOW(),%s)""",
                               (p.bbid, p.kind, p.medium, p.association, p.place, p.county,
                                p.dedication, p.location, p.date, p.duration, p.tenor,
                                p.length, p.method, p.details, p.composer, p.footnotes,
                                p.composition, p.toHtml(), p.updated))
                for b, n, c in p.ringers:
                    cursor.execute("INSERT INTO ringers VALUES(%s,%s,%s,%s)",
                                   (p.bbid, b, n, c))
        finally:
            cursor.execute("UNLOCK TABLES")
    except pymysql.Error as e:
        logging.error("Error while opening or writing database: {}, {}".format(
            e.args[0], e.args[1]))
    finally:
        if cursor:
            cursor.close()
        if conn:
            conn.close()
        logging.info("Added or updated {} performances rung since {}".format(
            len(performances), fromDate or "(unknown date)"))


class Statistics:

    SIMPLE = ("association", "location", "method")

    def __init__(self):
        self.totals = collections.defaultdict(int)
        self.statstab =  collections.defaultdict(
            lambda: collections.defaultdict(
                lambda: collections.defaultdict(int)))

    STAGES = "Minimus Doubles Minor Triples Major Caters Royal Cinques Maximus".split()
    STAGE_RE = re.compile(r"\b(?:" + "|".join(STAGES) + r")\b", re.IGNORECASE)

    def extract_stage(s):
        if not s:
            return ''
        m = Statistics.STAGE_RE.search(s)
        return m.group(0).capitalize() if m else ''

    RINGER_NAME = re.compile(r"^(?:\w|[ '-])+")

    def record(self, perf):
        if perf.length < 1250:
            return
        self.totals[perf.medium] += 1
        self.totals["both"] += 1
        s = self.statstab[perf.medium]
        b = self.statstab["both"]
        for a in Statistics.SIMPLE:
            s[a][getattr(perf, a)] += 1
            b[a][getattr(perf, a)] += 1
        stage = Statistics.extract_stage(getattr(perf, "method"))
        s["stage"][stage] += 1
        b["stage"][stage] += 1
        for r in perf.ringers:
            m = Statistics.RINGER_NAME.match(r[1])
            name = m.group(0).strip() if m else r[1]
            s["ringer"][name] += 1
            b["ringer"][name] += 1
            if r[2]:
                s["conductor"][name] += 1
                b["conductor"][name] += 1

    LAST_WORD = re.compile(r"(?:\w|['-])+$")

    def last_word(s):
        if not s:
            return ''
        m = Statistics.LAST_WORD.search(s)
        return m.group(0) if m else ''

    def write(self, kind):
        print(kind + ":",
              str(self.totals["both"]) + "," +
              str(self.totals["tower"]) + "," +
              str(self.totals["hand"]))
        b = self.statstab["both"]
        t = self.statstab["tower"]
        h = self.statstab["hand"]
        for a in Statistics.SIMPLE:
            print(a + ",total,tower,hand")
            for val in sorted(sorted(b[a].items(),
                                     key=lambda x: x[0] or ''),
                              key=lambda y: y[1],
                              reverse=True):
                print('"' + str(val[0]) + '",' + str(val[1]) + "," +
                      str(t[a][val[0]]) + "," + str(h[a][val[0]]))
            print()
        print("\nstage,total,tower,hand")
        for st in Statistics.STAGES:
            n = b["stage"][st]
            if n:
                print(st + "," + str(b["stage"][st]) + "," +
                      str(t["stage"][st]) + "," + str(h["stage"][st]))
        print("\n" + str(len(b["ringer"])), "ringers,", len(b["conductor"]), "conductors")
        print("ringer,total,conducted,tower,conduted tower,hand,conducted hand")
        for r in sorted(sorted(sorted(b["ringer"].items(),
                                      key=lambda x: x[0] or ''),
                               key=lambda y: Statistics.last_word(y[0])),
                        key=lambda z: z[1],
                        reverse=True):
            print(r[0] + "," + str(r[1]) + "," + str(b["conductor"].get(r[0], 0)) + "," +
                  str(t["ringer"][r[0]]) + "," + str(t["conductor"][r[0]]) + "," +
                  str(h["ringer"][r[0]]) + "," + str(h["conductor"][r[0]]))
        print("\nDistinct ringers:")
        for r in sorted(sorted(b["ringer"].items(),
                                      key=lambda x: x[0] or ''),
                               key=lambda y: Statistics.last_word(y[0])):
            print(r[0])
        print("\n")


def main():
    locale.setlocale(locale.LC_ALL, "")
    if database:
        logging.basicConfig(format='%(asctime)s\t%(levelname)s\t%(message)s',
                            filename=LOGFILE,
                            level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument("--sort", "-s", choices=Performance.sort_styles.keys(), default="clapper",
                        help="style with which to sort performances")
    parser.add_argument("--from", "-f", dest="fromD", metavar="DATE",
                        help="earliest date (rung) for which to extract performances")
    parser.add_argument("--to", "-t", dest="toD", metavar="DATE",
                        help="last date (rung) for which to extract performances")
    parser.add_argument("--changed_since", "-F", dest="changedSinceD", metavar="DATE",
                        help="earliest date (added or changed) from which to extract reports")
    parser.add_argument("--changed_before", "-T", dest="changedBeforeD", metavar="DATE",
                        help="extract only reports added or changed before this date")
    parser.add_argument("--days", "-d", type=int,
                        help="number of days before today from to start extracting performances")
    parser.add_argument("--json", "-j", dest="jsonOut", action="store_true",
                        help="output performances as json")
    parser.add_argument("--statistics", "-S", dest="statistics", action="store_true",
                        help="adds some summary statistical information after the reports")
    parser.add_argument("--verbose", "-v", dest="verbose", action="store_true",
                        help="prints the queries sent to Bellboard as they are sent")

    options = parser.parse_args()
    if options.days and options.fromD:
        warnings.warn("When --from is supplied --days is ignored.")
    fromDate = datetime.date.today() - datetime.timedelta(days=(options.days or DAYSBACK))
    toDate = datetime.date.today()
    if options.fromD:
        fromDate = parseDate(options.fromD)
        if not fromDate:
            raise ValueError("Can't understand from date {}".format(options.fromD))
    if options.toD:
        toDate = parseDate(options.toD)
        if not toDate:
            raise ValueError("Can't understand to date {}".format(options.toD))
    if options.changedSinceD:
        changedSince = parseDate(options.changedSinceD)
        if not changedSince:
            raise ValueError("Can't understand changed_since date {}".format(options.changedSinceD))
    else:
        changedSince = None
    if options.changedBeforeD:
        changedBefore = parseDate(options.changedBeforeD)
        if not changedBefore:
            raise ValueError("Can't understand changed_before date {}".format(options.changedBeforeD))
    else:
        changedBefore = None
    params = {"from": str(fromDate)}
    if len(sys.argv) == 1 and database:
        try:
            performances = [Performance(p)
                            for p in filter(isNorthAmerican, getPerformances(params, options.verbose))]
        except Exception as e:
            logging.error("Couldn't get North American performances since {}: {}".format(
                fromDate, str(e)))
            exit(1)
        replacePerformances(fromDate, performances)
    else:
        print("\n\n")
        if toDate:
            params["to"] = str(toDate)
        if changedSince:
            params["changed_since"] = str(changedSince)
        if changedBefore:
            params["changed_before"] = str(changedBefore)
        inPeals = True
        recent_perfs = sorted([Performance(p)
                               for p in filter(isNorthAmerican, getPerformances(params, options.verbose))],
                              key=Performance.sort_styles[options.sort])
        if options.jsonOut:
            print(json.dumps([p.__dict__ for p in recent_perfs], default=str, indent=4))
            return

        peals = [p for p in recent_perfs if p.kind == "peal"]
        quarters = [p for p in recent_perfs if p.kind == "quarter"]
        touches = [p for p in recent_perfs if p.kind == "touch"]

        for p in peals:
            print(p.toText(), "\n")
        print("========================================================\n\n")

        if options.statistics:
            stats = Statistics()
            for p in peals:
                stats.record(p)
            stats.write("peals")
            print("========================================================\n\n")

        for q in quarters:
            print(q.toText(), "\n")
        print("========================================================\n\n")

        if options.statistics:
            stats = Statistics()
            for q in quarters:
                stats.record(q)
            stats.write("quarters")
            print("========================================================\n\n")

        for t in touches:
            print(t.toText(), "\n")

if __name__ == '__main__':
    main()
